import React, { useState } from 'react';
import './App.css';
import Movieslist from './component/movieslist';
import Nav from "./component/nav"

function App() {

  const [numMovies, setNumMovies] = useState('');

  const numerOfMovies = (number) => {
    setNumMovies(number)
  }
  return (
    <div className="App">
      <Nav  numMovies={numMovies} />
      <Movieslist numerOfMovies={numerOfMovies} />
    </div>
  );
}

export default App;
