import React, { useState } from 'react'
import { v4 as uuidv4 } from 'uuid';
const AddNewMovie = ({ AddNewMovie }) => {

    const [title, setTitle] = useState('');
    const [price, setPrice] = useState('');
    const [director, setDirector] = useState('');
    const [body, setBody] = useState('');
    const add = (e) => {

        e.preventDefault();
        let NewMovieObject = {
            title,
            id: uuidv4(),
            price,
            director,
            body
        }
        AddNewMovie(NewMovieObject)
        setTitle('');
        setPrice('');
        setDirector('');
        setBody('');
    }

    return (
        <div className='addmovieBox'>
            <h1>Movie List</h1>
            <p>(React.js)Using Compnants, Array, Props (function & Data passsing Parrent to child and child to parrent ) Functions , Hooks</p>
            <p>Develop By : Usman Islam</p>
            <form onSubmit={add}>
                <div className="inputBox">
                    <label htmlFor="">Title</label>
                    <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} className="addInput" required />
                </div>
                <div className="inputBox">
                    <label htmlFor="">Price</label>
                    <input type="text" value={price} onChange={(e) => setPrice(e.target.value)} className="addInput" required />
                </div>
                <div className="inputBox">
                    <label htmlFor="">Director</label>
                    <input type="text" value={director} onChange={(e) => setDirector(e.target.value)} className="addInput" required />
                </div>
                <div className="inputBox">
                    <label htmlFor="">Comments</label>
                    <input type="text" value={body} onChange={(e) => setBody(e.target.value)} className="addInput" required />
                </div>

                <button type="submit" className='addbtn'>Add New Movie</button>
            </form>


        </div>
    )
}

export default AddNewMovie
