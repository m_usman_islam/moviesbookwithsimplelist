import React from 'react'

const Movie = ({ id, title, price, director, body ,deleteMovie}) => {
    return (
        <div>
            <div className="moviesBox">
                <p>{id}</p>
                <h1>{title}</h1>
                <h2>${price}</h2>
                <p>{director}</p>
                <p>{body}</p>

                <a href="!#" onClick={()=>deleteMovie(id)} className="deletebtn">Delete</a>
            </div>
        </div>
    )
}

export default Movie
