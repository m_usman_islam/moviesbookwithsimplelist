import React from 'react'

const nav = ({ numMovies }) => {
    return (
        <div className="nav">
            <p className="logoName">Movies Book</p>
            <p className="NumberOfMovies">Total Movies :{numMovies}</p>
        </div>
    )
}

export default nav
