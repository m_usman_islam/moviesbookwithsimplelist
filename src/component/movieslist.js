import React, { useState, useEffect } from 'react'
import Movie from './movie'
import AddNewMovieC from './AddNewMovie';
const Movieslist = ({ numerOfMovies }) => {
    const [movies, setMovies] = useState([
        {
            title: "Harry Potter",
            id: 1,
            price: "$150",
            director: "Mark Thoums",
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."

        },
        {
            title: "Inception",
            id: 2,
            price: "$120",
            director: "Carry John",
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."

        },
        {
            title: "pirate of the Caribbian",
            id: 3,
            price: "$175",
            director: "James Bond",
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."

        },
        {
            title: "Loggan",
            id: 4,
            price: "$190",
            director: "Brillen Yaki",
            body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."

        },
    ])

    const deleteMovie = (id) => {
        let delmovie = movies.filter(movie => {
            return movie.id !== id
        })
        setMovies(delmovie)

    }

    const AddNewMovie = (newmovie) => {
        let NewMoviesList = [newmovie, ...movies];
        setMovies(NewMoviesList);

    }

    useEffect(() => {
        numerOfMovies(movies.length);
    }, [movies, numerOfMovies])



    return (
        <div>
            <AddNewMovieC AddNewMovie={AddNewMovie} />
            {
                movies.map(movie => {
                    return (<Movie id={movie.id} title={movie.title} price={movie.price} director={movie.director} body={movie.body} key={movie.id} deleteMovie={deleteMovie} />)
                })
            }

        </div>
    );
}

export default Movieslist
